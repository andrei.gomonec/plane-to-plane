using UnityEngine;
using UnityEngine.UI;

public class MovePlayer : MonoBehaviour
{
    [SerializeField] private ParticleSystem gameOverParticl;
    [SerializeField] private Text scoreText;
    [SerializeField] private GameObject pointTOP;
    [SerializeField] private GameObject pointBOT;
    [SerializeField] private ParticleSystem off;
    [SerializeField] private AudioSource destroy;
    [SerializeField] private float speed;

    private AudioSource touchMusic;
    private int score = 0;
    private int bestScore;
    private Transform transformPlayer;
    private bool isTop = true;
    private bool touchTop;
    private Vector2 endPointPosition;

    public bool isGame = true;

    private void Start()
    {
        touchMusic = GetComponent<AudioSource>();
        transformPlayer = GetComponent<Transform>();
        PlayerPrefs.SetInt("Score", score);
        generateAPoint();
    }

    private void Update()
    {
        scoreText.text = ("Score: " + score);
        if (Input.GetMouseButtonUp(0))
        {
            touchMusic.Play();
            generateAPoint();
        }
        transformPlayer.position = Vector2.MoveTowards(transform.position, endPointPosition, speed * Time.deltaTime);
    }

    private void ScorePref()
    {
        bestScore = PlayerPrefs.GetInt("BestScore");
        PlayerPrefs.SetInt("Score", score);
        if (score > bestScore)
        {
            bestScore = score;
            PlayerPrefs.SetInt("BestScore", bestScore);
        }
    }

    private void generateAPoint()
    {
        float horizontal = Random.Range(-4.7f, 4.7f);
        float vertical = 3.68f;
        if (isTop)
        {
            endPointPosition = new Vector2(horizontal, vertical);
            pointTOP.transform.position = endPointPosition;
            isTop = false;
        }
        else
        {
            endPointPosition = new Vector2(horizontal, -vertical);
            pointBOT.transform.position = endPointPosition;
            isTop = true;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Point")
        {
            Score();
            generateAPoint();
        }
        if (collision.gameObject.tag == "PointBot")
        {
            Score();
            generateAPoint();
        }

        if (collision.gameObject.tag == "Enemy")
        {
            destroy.Play();
            isGame = false;
            Destroy(gameObject);
            Instantiate(gameOverParticl, transformPlayer.position, Quaternion.identity);
            gameOverParticl.Play();
            ScorePref();
        }
    }

    private void Score()
    {
        if(isTop == false && touchTop == true)
        {
            score++;
            touchTop = false;
        }
        if (isTop == true && touchTop == false)
        {
            score++;
            touchTop = true;
        }
    }
}
