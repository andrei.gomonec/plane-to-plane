using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    [SerializeField] private GameObject platformEnemy;
    [SerializeField] private float minDelay, maxDelay;
    private float nextLaunchTime;

    private void Update()
    {
        if (Time.time > nextLaunchTime)
        {
            float horizontal = -10.0f;
            float vertical = Random.Range(-2.6f, 2.6f);
            Vector2 platformPosition = new Vector2(horizontal, vertical);
            nextLaunchTime = Time.time + Random.Range(minDelay, maxDelay);
            Instantiate(platformEnemy, platformPosition, Quaternion.identity);
        }
    }
}
