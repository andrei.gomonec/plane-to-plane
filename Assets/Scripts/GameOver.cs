using UnityEngine;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
    [SerializeField] private Text scoreText;
    [SerializeField] private Text bestScoreText;

    private int score;
    private int bestScore;

    private void Start()
    {
        score = PlayerPrefs.GetInt("Score");
        bestScore = PlayerPrefs.GetInt("BestScore");

        scoreText.text = ("Score: " + score);
        bestScoreText.text = ("Best: "+ bestScore);
    }

    public void OnButton()
    {
        Application.LoadLevel("SampleScene");
    }
}
