using UnityEngine;

public class Platform : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] GameObject del;

    private Vector2 ended;

    private void Start()
    {
        speed = Random.Range(1.5f, 2f);
        float lenght = Random.Range(2.5f, 3.5f);
        transform.localScale = new Vector3(lenght, transform.localScale.y, transform.localScale.z);
        float rotation = Random.Range(35, 145);
        ended = new Vector2(50f, transform.position.y);
        transform.localEulerAngles = new Vector3(transform.rotation.x, transform.rotation.y, rotation);
    }

    private void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, ended, speed * Time.deltaTime);
    }
}
