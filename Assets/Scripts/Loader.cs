using UnityEngine;
using UnityEngine.SceneManagement;

public class Loader : MonoBehaviour
{
    public MovePlayer move;
    //private bool dead = false;

    void Update()
    {
        if(move.isGame == false /*|| dead == true*/)
        {
            //dead = true;
            Invoke("Load", 2f);
        }
    }

    private void Load()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
